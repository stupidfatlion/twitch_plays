__author__ = "Shum"

import socket
from send_key import Press
from time import sleep
import re
import CONFIG

def connect_irc(USERNAME, PASSWORD, CHANNEL, BOT_OWNER, SERVER):
    irc = socket.socket()
    irc.connect((SERVER, 6667))
    irc.send('PASS ' + PASSWORD + '\r\n')
    irc.send('USER ' + USERNAME + ' 0 * :' + BOT_OWNER + '\r\n')
    irc.send('NICK ' + USERNAME + '\r\n')
    irc.send('JOIN ' + CHANNEL + '\r\n')
    return irc

def whitelisted(message):
    return len(message)==1 and message in CONFIG.WHITELIST

def main():
    pattern = re.compile(r":[a-zA-Z0-9_]*\![a-zA-Z0-9_]*@[a-zA-Z0-9._]* PRIVMSG #[a-zA-Z0-9_]* :")
    irc = connect_irc(CONFIG.USERNAME, CONFIG.PASSWORD, CONFIG.CHANNEL, CONFIG.BOT_OWNER, CONFIG.SERVER)
    while True:
        message = None
        data = irc.recv(2048) #buffer size, I don't know what's the max limit for twitch, but this is a safe bet
        if pattern.match(data):
            message = pattern.split(data)[1].strip()
            print message
        if message and whitelisted(message):
            print "Pressing " + message
            Press(message)
            sleep(.1)
        if data.find("PING") != -1:
            irc.send(data.replace("PING", "PONG"))

if __name__ == "__main__":
    main()